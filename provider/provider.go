package provider

import (
	"encoding/json"
	"fmt"
	"golang.org/x/crypto/ssh"
	"net"
	"net/http"
	"strings"
	"sync"
	"time"
)

var Init = provider{
	"defaultProvider",
}

type response struct {
	body map[string]string
}

type provider struct {
	name string
}

func (pr provider) Scan(ip string) string {
	passwordList := []string{"password", "pass", "root", "admin", "alpine"}
	loginList := []string{"admin", "root", "user"}
	resultMap := response{}
	resultMap.body = make(map[string]string)

	var wg sync.WaitGroup

	wg.Add(2)
	go sshScan(ip, resultMap, &wg, loginList, passwordList)
	go httpBasicAuth(ip, resultMap, &wg, loginList, passwordList)
	wg.Wait()

	r, _ := json.Marshal(resultMap.body)

	return string(r)
}

func (pr provider) GetName() string {
	return pr.name
}

func sshScan(ip string, resultMap response, wg *sync.WaitGroup, loginList []string, passwordList []string) {
	defer wg.Done()

	if scanPort(ip, 22, time.Second*3) {
		config := ssh.ClientConfig{}
		var err error

		for _, login := range loginList {
			for _, password := range passwordList {
				config.User = login
				config.Auth = []ssh.AuthMethod{
					ssh.Password(password),
				}

				_, err = ssh.Dial("tcp", ip+":22", &config)
				if err != nil {
					continue
				}

				resultMap.body["ssh"] = login + "@" + password
			}
		}
	}
}

func httpBasicAuth(ip string, resultMap response, wg *sync.WaitGroup, loginList []string, passwordList []string) {
	defer wg.Done()

	scanner := func(addr, login, pass string) bool {
		client := &http.Client{}
		req, err := http.NewRequest("GET", addr, nil)
		if err != nil {
			return false
		}

		req.SetBasicAuth(login, pass)
		resp, err := client.Do(req)
		if err != nil || resp.StatusCode != 200 {
			return false
		}

		return true
	}

	isFound := false

	if scanPort(ip, 80, time.Second*3) {
		for _, login := range loginList {
			for _, pass := range passwordList {
				if scanner("http://"+ip, login, pass) {
					resultMap.body["http" + login] = login + "@" + pass
					isFound = true
					break
				}

				if isFound {
					isFound = false
					break
				}
			}
		}
	}

	if scanPort(ip, 443, time.Second*3) {
		for _, login := range loginList {
			for _, pass := range passwordList {
				if scanner("https://"+ip, login, pass) {
					resultMap.body["https"] = login + "@" + pass
					isFound = true
					break
				}
			}

			if isFound {
				isFound = false
				break
			}
		}
	}
}

func scanPort(ip string, port int, timeout time.Duration) bool {
	target := fmt.Sprintf("%s:%d", ip, port)
	conn, err := net.DialTimeout("tcp", target, timeout)

	if err != nil {
		if strings.Contains(err.Error(), "too many open files") {
			time.Sleep(timeout)
			scanPort(ip, port, timeout)
		}
		return false
	}

	_ = conn.Close()

	return true
}
