package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net"
	"os"
	"sync"
)

type Provider interface {
	Scan(str string) string
	GetName() string
}

type IpResponse struct {
	Ip map[string]ProviderResponse
}

type ProviderResponse struct {
	Provider map[string]string
}

func main() {
	var hostname string
	var ip string
	var workerCount int
	var wg sync.WaitGroup

	flag.StringVar(&hostname, "host", "", "hostname")
	flag.StringVar(&ip, "ip", "", "ip or cidr")
	flag.IntVar(&workerCount, "c", 1, "count of concurrent workers")
	flag.Parse()

	ipList := initIpList(hostname, ip)
	ipCount := len(ipList)

	ipChan := make(chan string, ipCount)
	resultChan := make(chan IpResponse, ipCount)

	for _, ip := range ipList {
		ipChan <- ip
	}
	close(ipChan)

	wg.Add(workerCount)
	for i := 1; i <= workerCount; i++ {
		go scan(&wg, ipChan, resultChan)
	}
	wg.Wait()

	initResponse(resultChan, ipCount)
}

func initResponse(resultChan <-chan IpResponse, ipCount int) {
	var response []byte
	if len(resultChan) > 0 {
		var rList []IpResponse
		for i := 0; i < ipCount; i++ {
			rList = append(rList, <-resultChan)
		}
		response, _ = json.Marshal(rList)
	}

	_, _ = os.Stdout.Write(response)
}

func initIpList(hostname, ip string) []string {
	var ipList []string
	if !isEmpty(hostname) {
		ips, err := net.LookupIP(hostname)
		if err != nil {
			fmt.Println("error on parse hostname")
		} else {
			for _, i := range ips {
				ipList = append(ipList, i.String())
			}
		}
	}

	if !isEmpty(ip) {
		if isCIDR(ip) {
			ips, err := getIpList(ip)
			if err != nil {
				fmt.Println("error on parse cidr")
			} else {
				ipList = append(ipList, ips...)
			}
		} else {
			ipList = append(ipList, ip)
		}
	}

	return ipList
}

func getIpList(cidr string) ([]string, error) {
	ip, ipnet, err := net.ParseCIDR(cidr)
	if err != nil {
		return nil, err
	}

	var ips []string
	i := ip.Mask(ipnet.Mask)
	for ; ipnet.Contains(i); inc(i) {
		ips = append(ips, i.String())
	}

	return ips, nil
}

func isEmpty(str string) bool {
	return len(str) == 0
}

func isCIDR(s string) bool {
	for i := range s {
		if s[i:i+1] == "/" {
			return true
		}
	}

	return false
}

func inc(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

func scan(wg *sync.WaitGroup, ipChan <-chan string, resultChan chan<- IpResponse) {
	for ip := range ipChan {
		ipMap := IpResponse{
			Ip: map[string]ProviderResponse{},
		}
		providerMap := ProviderResponse{
			Provider: map[string]string{},
		}

		for _, provider := range providerList {
			providerMap.Provider[provider.GetName()] = provider.Scan(ip)
		}

		ipMap.Ip[ip] = providerMap
		resultChan <- ipMap
	}

	wg.Done()
}
